# pyROOT MemoryMonitor plotting scripts

**MemoryMonitor:**

* Running inside every transformation and is also invoked by PandaPilot. Monitors a process memory and I/O
usage based on _/proc_.
* The source code in all athena releases: [code in athena master](https://gitlab.cern.ch/atlas/athena/tree/master/Control/AthenaMP/src/memory-monitor)
* Specific notes on the I/O part: [twiki](https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/MemoryMonitorIO)

**NetworkMonitor nw2:**
 
* Prototype code to monitor process network usage.
* Source code: [code in private gitlab](https://gitlab.cern.ch/fhoenig/nw2/tree/master)
* More detailed description: [twiki](https://twiki.cern.ch/twiki/bin/view/Sandbox/NetworkMonitoring)
