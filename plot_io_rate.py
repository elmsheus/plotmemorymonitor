#!/usr/bin/env python

from ROOT import *

from ROOT import *
from ROOT import TCanvas, TGraph
from ROOT import gROOT
from math import sin
from array import array
import glob, os, sys, getopt
import numpy


def main(argv):
  
  # Maxiumum Y-axis value 
  ymax = 2
  #jobid = 3309344620
  jobid = "q431_1_off"

  try:
    opts, args = getopt.getopt(argv,"hy:",["ymax="])
  except getopt.GetoptError:
    print 'plot_mem.py [-y <NUM>]'
    sys.exit(2)
  for opt, arg in opts:
    if opt == '-h':
      print 'plot_mem.py [-y <NUM>]'
      sys.exit()
    elif opt in ("-y", "--ymax"):
      ymax = int(arg)

  c1 = TCanvas( 'c1', 'job I/O', 200, 10, 700, 500 )

  searchdir = '.'
  os.chdir(searchdir)
  allFiles = []

  files = glob.glob("mem.full.*")
  files.sort(key=os.path.getmtime)
  for file in files:
    allFiles.append(file)

  pssAll = array('d',[])
  rssAll = array('d',[])
  vmemAll = array('d',[])
  swapAll = array('d',[])
  timeAll = array('d',[])
  rcharAll = array('d',[])
  wcharAll = array('d',[])
  rbytesAll = array('d',[])
  wbytesAll = array('d',[])
  rcharRate = array('d',[])
  wcharRate = array('d',[])
  rbytesRate = array('d',[])
  wbytesRate = array('d',[])
  timeN = 0
  walltime = 0
  deltatime = 0

  allLines = []
  startTRF = []
  for filename in allFiles:
    f = open(filename, 'r')
    print filename
    lines = f.readlines()
    if len(lines)<=1:
      continue
    startTime = lines[1].split()[0]
    startTRF.append(float(startTime))
    allLines += lines 
    f.close()

  for line in allLines:
    if not line.startswith("15"):
        continue
    time, vmem, pss, rss, swap, rchar, wchar, rbytes, wbytes = line.split()
    if timeN==0:
        timeN = float(time)
    if float(time)-timeN>walltime:
      walltime = float(time)-timeN

    if deltatime==0:
      deltatime = float(time) - 1
    else:
      deltatime = float(time) - timeNM1

    timeNM1 = float(time)
      
    timeAll.append(float(time)-timeN)
    rcharAll.append(float(rchar)/1024./1024.)
    wcharAll.append(float(wchar)/1024./1024.)
    rbytesAll.append(float(rbytes)/1024./1024.)
    wbytesAll.append(float(wbytes)/1024./1024.)

    if len(rcharAll)>1:
      rcharRate.append( (float(rchar)/1024./1024. - rcharAll[-2] ) / deltatime )
    if len(wcharAll)>1:
      wcharRate.append( (float(wchar)/1024./1024. - wcharAll[-2] ) / deltatime )
    if len(rbytesAll)>1:
      rbytesRate.append( (float(rbytes)/1024./1024. - rbytesAll[-2] ) / deltatime )
    if len(wbytesAll)>1:
      wbytesRate.append( (float(wbytes)/1024./1024. - wbytesAll[-2] ) / deltatime )

  durations = array('d',[])
  ydurations = array('d',[])

  for time in startTRF:
    duration = time - timeN
    durations.append(duration)
    ydurations.append(8)

  durations.append(walltime)
  ydurations.append(8)

  rbytesMax = numpy.amax(rbytesRate)
  rcharMax = numpy.amax(rcharRate)
  print "rbytesMax = %2.2f MB/s " % rbytesMax
  print "rcharMax = %2.2f MB/s " % rcharMax

  print durations

  mg = TMultiGraph()
    
  gr1 = TGraph(len(timeAll), timeAll, rbytesRate)
  gr2 = TGraph(len(timeAll), timeAll, wbytesRate)
  gr3 = TGraph(len(timeAll), timeAll, rcharRate)
  gr4 = TGraph(len(timeAll), timeAll, wcharRate)
  gr5 = TGraph(len(durations), durations, ydurations)

  gr1.SetMinimum(0)
  gr1.SetMaximum(ymax)
  gr1.GetXaxis().SetTitle( 'time [s]' )
  gr1.GetYaxis().SetTitle( 'rbytes [MB/s]' )

  gr2.SetMarkerColor(3);
  gr2.SetMarkerStyle(21);

  gr3.SetMarkerColor(4);
  gr3.SetMarkerStyle(22);

  gr4.SetMarkerColor(5);
  gr4.SetMarkerStyle(23);

  gr5.SetMarkerColor(1);
  gr5.SetMarkerStyle(2);

  mg.Add(gr1)
  mg.Add(gr2)
  mg.Add(gr3)
  mg.Add(gr4)
  mg.Add(gr5)

  mg.SetMinimum(0)
  mg.SetMaximum(ymax)

  mg.Draw("ALP")
  #mg.SetTitle( 'Memory RAWtoALL q431' )  
  mg.SetTitle( 'I/O throughput' )  
  mg.GetXaxis().SetTitle( 'time [s]' )
  #mg.GetYaxis().SetTitle( 'RAWtoALL q431 Memory [GB]' )
  mg.GetYaxis().SetTitle( 'job I/O rate [MB/s]' )

  mg.GetXaxis().SetNdivisions(00007)
  
  #gr1.SetMinimum(0)
  #gr1.SetMaximum(20)

  leg = TLegend (0.65, 0.70, 0.95, 0.90)
  leg.SetFillColor(0)
  leg.SetFillStyle(0)
  leg.SetTextFont(42)
  leg.SetTextSize(0.035)
  leg.SetBorderSize(0)
  leg.SetNColumns(2)
  leg.SetHeader("#splitline{walltime = %2.2f h}{max rbytes = %2.2f MB/s}" % (walltime/3600.,rbytesMax))

  leg.AddEntry(gr3, 'rchar', "P")
  leg.AddEntry(gr2, 'wbytes', "P")
  leg.AddEntry(gr1, 'rbytes', "P")
  leg.AddEntry(gr4, 'wchar', "P")
  leg.AddEntry(0, "job %s"%jobid, "")

  #pt = TPaveText(0,0,1000,10)
  #pt.AddText("walltime = %s s" % (walltime) )
  #pt.AddText("maxPSS = %2.2f GB" % (pssMax) )
  #pt.Draw()

  leg.Draw()
  
  c1.Update()
  c1.Print("io_rate_%s.pdf" %jobid)
  raw_input("Press Enter to continue...")
      
if __name__ == "__main__":

  gROOT.LoadMacro("$PWD/AtlasStyle.C")
  SetAtlasStyle()

  main(sys.argv[1:])
  
