
#!/usr/bin/env python

from ROOT import *

from ROOT import *
from ROOT import TCanvas, TGraph
from ROOT import gROOT
from math import sin
from array import array
import glob, os, sys, getopt
import numpy


def main(argv):
  
  # Maxiumum Y-axis value 
  ymax = 1000
  #jobid = 3309344620
  jobid = "q431"

  try:
    opts, args = getopt.getopt(argv,"hy:",["ymax="])
  except getopt.GetoptError:
    print 'plot_mem.py [-y <NUM>]'
    sys.exit(2)
  for opt, arg in opts:
    if opt == '-h':
      print 'plot_net.py [-y <NUM>]'
      sys.exit()
    elif opt in ("-y", "--ymax"):
      ymax = int(arg)

  c1 = TCanvas( 'c1', 'network i/o', 200, 10, 700, 500 )

  searchdir = '.'
  os.chdir(searchdir)
  allFiles = []

  #files = glob.glob("mem.full.*")
  files = glob.glob("nw2.log")
  files.sort(key=os.path.getmtime)
  for file in files:
    allFiles.append(file)

  netoutAll = array('d',[])
  netinAll = array('d',[])
  timeAll = array('d',[])
  timeN = 0
  walltime = 0

  allLines = []
  startTRF = []
  for filename in allFiles:
    f = open(filename, 'r')
    print filename
    lines = f.readlines()
    if len(lines)<=1:
      continue
    startTime = lines[1].split()[0]
    startTRF.append(float(startTime))
    allLines += lines 
    f.close()

  time0 = 0
  for line in allLines:
    if not line.startswith("15"):
        continue
    pss = rss = swap = rchar = wchar = rbytes = wbytes = 0
    #time, vmem, pss, rss, swap, rchar, wchar, rbytes, wbytes = line.split()
    time, netin, netout = line.split()
    if timeN==0:
        timeN = float(time)
    if float(time)-timeN>walltime:
      walltime = float(time)-timeN
    # Skip if already entry with same time
    if time == time0:
      continue
    else:
      time0 = time
      
    timeAll.append(float(time)-timeN)
    netoutAll.append(float(netout)/1024./1024.)
    netinAll.append(float(netin)/1024./1024.)

  print "timebins = %s " %(len(timeAll))
  
  durations = array('d',[])
  ydurations = array('d',[])

  for time in startTRF:
    duration = time - timeN
    durations.append(duration)
    ydurations.append(8)

  durations.append(walltime)
  ydurations.append(8)

  netinMax = numpy.amax(netinAll)
  print "netinMax = %2.2f GB " % netinMax

  netoutMax = numpy.amax(netoutAll)
  print "netoutMax = %2.2f GB " % netoutMax

  print durations

  mg = TMultiGraph()
    
  gr1 = TGraph(len(timeAll), timeAll, netoutAll)
  gr2 = TGraph(len(timeAll), timeAll, netinAll)
  gr3 = TGraph(len(durations), durations, ydurations)

  gr1.SetMinimum(0)
  gr1.SetMaximum(ymax)
  gr1.GetXaxis().SetTitle( 'time [s]' )
  gr1.GetYaxis().SetTitle( 'Net I/O [MB]' )
  gr1.SetMarkerColor(3);

  gr2.SetMarkerColor(4);
  gr2.SetMarkerStyle(21);

  gr3.SetMarkerColor(1);
  gr3.SetMarkerStyle(22);

  mg.Add(gr1)
  mg.Add(gr2)
  mg.Add(gr3)

  mg.SetMinimum(0)
  mg.SetMaximum(ymax)

  mg.Draw("ALP")
  mg.SetTitle( 'Net I/O usage' )  
  mg.GetXaxis().SetTitle( 'time [s]' )
  mg.GetYaxis().SetTitle( 'Network I/O [MB]' )

  mg.GetXaxis().SetNdivisions(00007)
  
  #gr1.SetMinimum(0)
  #gr1.SetMaximum(20)

  leg = TLegend (0.60, 0.70, 0.95, 0.90)
  leg.SetFillColor(0)
  leg.SetFillStyle(0)
  leg.SetTextFont(42)
  leg.SetTextSize(0.035)
  leg.SetBorderSize(0)
  leg.SetNColumns(2)
  leg.SetHeader("#splitline{walltime = %2.2f h}{max in=%2.2f MB, out=%2.2f MB}" % (walltime/3600.,netinMax,netoutMax))

  leg.AddEntry(gr2, 'net in', "P")
  leg.AddEntry(gr1, 'net out', "P")
  leg.AddEntry(0, "job %s"%jobid, "")

  #pt = TPaveText(0,0,1000,10)
  #pt.AddText("walltime = %s s" % (walltime) )
  #pt.AddText("maxPSS = %2.2f GB" % (pssMax) )
  #pt.Draw()

  leg.Draw()
  
  c1.Update()
  #c1.Print("memory_graph.pdf")
  c1.Print("network_%s.pdf" %jobid)
  #c1.Print("network_%s.png" %jobid)
  raw_input("Press Enter to continue...")
      
if __name__ == "__main__":

  gROOT.LoadMacro("$HOME/AtlasStyle.C")
  SetAtlasStyle()

  main(sys.argv[1:])
  
